import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class HttpserviceService {

  constructor(
    private http:HttpClient
  ) { }

  getData(url:string){
    return this.http.get<User>(url);
  }
}
