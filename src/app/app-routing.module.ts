import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {Tab1Component} from './tab1/tab1.component';
import {Tab2Component} from './tab2/tab2.component';
const routes: Routes = [
  {path:'home', component: Tab1Component},
  {path:'contactus/:id',component:Tab2Component}
];

@NgModule({
  //imports: [RouterModule.forRoot(routes,{useHash: true})],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
