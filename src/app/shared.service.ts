import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  sharedData:string[]=[];
  constructor() { 

  }

  insertData(data:string){
    this.sharedData.unshift(data);
  }
}
