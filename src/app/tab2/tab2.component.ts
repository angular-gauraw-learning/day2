import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {HttpserviceService} from '../httpservice.service';
import { environment } from '../../environments/environment';
import {User} from '../user';
import { SharedService } from '../shared.service';


@Component({
  selector: 'app-tab2',
  templateUrl: './tab2.component.html',
  styleUrls: ['./tab2.component.css'],
  providers: []
})
export class Tab2Component implements OnInit {

  contactid:number;
  user: User;
  dataList: string[] = [];


  constructor(
    private route:ActivatedRoute,
    private httpService:HttpserviceService,
    private sharedService:SharedService
  ) { }

  ngOnInit() {
    
    this.dataList = this.sharedService.sharedData;
    console.log(this.dataList);
    this.route.params.subscribe(params =>{
      this.contactid = params['id'];

      this.httpService.getData(environment.serverurl+"/"+this.contactid).subscribe(
        data =>{
          this.user = data;
          console.log(data);
        },
        error =>{
          console.log(error.status);
        }
      )
    });
    console.log("this id which is passed is "+this.contactid);
  }

}
