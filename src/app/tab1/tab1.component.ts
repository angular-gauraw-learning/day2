import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-tab1',
  templateUrl: './tab1.component.html',
  styleUrls: ['./tab1.component.css'],
  providers:[]
})
export class Tab1Component implements OnInit {

  data:string="";
  constructor(
    private sharedService:SharedService
  ) { }

  ngOnInit() {
  }

  addValue(){
    this.sharedService.insertData(this.data);
    console.log(this.sharedService.sharedData);
  }

}
